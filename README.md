# laravel-app
Basado en la explicacion de  
https://ricardogeek.com/configurar-laravel-nginx-y-mysql-con-docker-compose/  

(En el source hay un clone de laravel:  
git clone https://github.com/laravel/laravel.git src)  

```
git clone https://github.com/grodrigo/laravel-app.git  
cd laravel-app  
```

Correr un composer install con una imagen docker del composer:  
```
cd src  
docker run --rm -v $(pwd):/app composer install  
```

Configuraciones de la db tienen que estar en .env (para el docker) y en src/.env (para la aplicacion).  
Copiar los .env.example a .env

```
cd ..
cp .env.example .env
cp ./src/.env.example ./src.env
```

Crear la key para laravel
```
docker-compose up -d
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan config:cache
```

Probar entrar a la db con el user dev_user, pass dev_pass y ver si esta la db laravel, sino correr estos comandos para crearlo y dar permisos
```
docker-compose exec db bash
mysql -u root -p root (y poner password root)
mysql> GRANT ALL ON laravel.* TO 'dev_user'@'%' IDENTIFIED BY 'dev_pass';
mysql> FLUSH PRIVILEGES;
mysql> EXIT;
```

docker-compose exec app php artisan migrate

